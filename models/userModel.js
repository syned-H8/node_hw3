const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    required: true,
  },
});

module.exports.User = mongoose.model('User', userSchema);
