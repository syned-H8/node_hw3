const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

module.exports.getUserInfo = async (req, res) => {
  const {email} = req.user;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email ${email} found!`});
  }

  res.status(200)
      .json({
        user: {
          _id: user._id,
          email: user.email,
          created_date: user.created_date,
        },
      });
};

module.exports.deleteUser = async (req, res) => {
  const {_id, email} = req.user;

  const user = await User.findById(_id);

  if (!user) {
    return res.status(400)
        .json({message: `No user with email ${email} found!`});
  }

  await User.findByIdAndDelete(_id);

  res.status(200)
      .json({message: 'Profile deleted successfully'});
};

module.exports.submitPassword = async (req, res, next) => {
  const {_id} = req.user;
  const {oldPassword} = req.body;
  const user = await User.findById(_id);

  bcrypt.compare(oldPassword, user.password, function(err, result) {
    if (err) {
      return res.status(500).json({message: 'Server error!'});
    }

    if (!result) {
      return res.status(400).json({message: 'Wrong password!'});
    }

    next();
  });
};

module.exports.changePassword = async (req, res) => {
  const {_id, email} = req.user;
  const {newPassword} = req.body;

  const user = await User.findById(_id);

  if (!user) {
    return res.status(400)
        .json({message: `No user with email ${email} found!`});
  }

  await User.findByIdAndUpdate(
      _id,
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );

  res.status(200).json({message: 'Password changed successfully'});
};
