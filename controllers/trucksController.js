const {Truck} = require('../models/truckModel');

module.exports.addTruck = async (req, res) => {
  const {_id} = req.user;
  const {type} = req.body;

  const truck = new Truck({
    created_by: _id,
    type: type.toUpperCase(),
  });

  await truck.save();

  res.json({message: 'Truck created successfully'});
};

module.exports.getUserTrucks = async (req, res) => {
  const {_id} = req.user;

  const trucks = await Truck.find({created_by: _id});

  res.json({trucks});
};

module.exports.getTruckById = async (req, res) => {
  const id = req.params.id;

  const truck = await Truck.findById(id);

  if (!truck) {
    return res.status(400)
        .json({message: `No truck with id: ${id} was found`});
  }

  res.json({truck});
};

module.exports.updateTruckInfo = async (req, res) => {
  const truckId = req.params.id;
  const newType = req.body.type.toLowerCase();

  await Truck.findByIdAndUpdate(truckId, {$set: {type: newType}});

  res.json({message: 'Truck details changed successfully'});
};

module.exports.deleteTruck = async (req, res) => {
  const truckId = req.params.id;

  await Truck.findByIdAndDelete(truckId);

  res.json({message: 'Truck deleted successfully'});
};

module.exports.assignTruck = async (req, res) => {
  const truckId = req.params.id;
  const userId = req.user._id;

  const assignedTruck = await Truck.findOne({assigned_to: userId});

  if (assignedTruck) {
    return res.status(400)
        .json({message: 'User can assign only one truck'});
  }

  const truck = await Truck.findOneAndUpdate({_id: truckId, created_by: userId},
      {$set: {assigned_to: userId}});

  if (!truck) {
    return res.status(400)
        .json({message: 'No available truck'});
  }
  res.json({message: 'Truck assigned successfully'});
};
