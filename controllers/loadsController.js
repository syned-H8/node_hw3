const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

module.exports.addLoad = async (req, res) => {
  const {_id} = req.user;
  const {
    name, payload, pickup_address: pickupAddress,
    delivery_address: deliveryAddress, dimensions,
  } = req.body;

  const load = new Load({
    name,
    payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions,
    created_by: _id,
  });

  await load.save();

  res.json({message: 'Load created successfully'});
};

module.exports.getLoads = async (req, res) => {
  const {role, _id: userId} = req.user;

  let {offset = 0, limit = 10, status} = req.query;

  limit = parseInt(limit) > 50 ? 50 : limit;

  const requestOptions = {
    skip: parseInt(offset),
    limit: parseInt(limit),
  };

  if (role === 'driver') {
    if (status) {
      status = status.toUpperCase();

      const truck = await Truck.findOne({assigned_to: userId});


      const loads = await Load.find(
          {assigned_to: truck._id, status},
          [],
          requestOptions,
      );

      if (!loads.length) {
        return res.status(400)
            .json({message: 'No loads were found'});
      }

      return res.json({loads});
    } else {
      const truck = await Truck.findOne({assigned_to: userId});

      const loads = await Load.find(
          {assigned_to: truck._id},
          [],
          requestOptions,
      );

      if (!loads.length) {
        return res.status(400)
            .json({message: 'No loads were found'});
      }

      return res.json({loads});
    }
  } else {
    if (status) {
      status = status.toUpperCase();

      const loads = await Load.find(
          {created_by: userId, status},
          [],
          requestOptions,
      );

      if (!loads.length) {
        return res.status(400)
            .json({message: 'No loads were found'});
      }

      return res.json({loads});
    } else {
      const loads = await Load.find(
          {created_by: userId},
          [],
          requestOptions,
      );

      if (!loads.length) {
        return res.status(400)
            .json({message: 'No loads were found'});
      }

      return res.json({loads});
    }
  }
};

module.exports.getActiveLoads = async (req, res) => {
  const {_id: userId} = req.user;

  const truck = await Truck.findOne({
    assigned_to: userId,
    status: {$ne: 'IS'},
  });

  if (!truck) {
    return res.status(400)
        .json({message: 'No active truck was found'});
  }

  const loads = await Load.find({
    assigned_to: truck._id,
    status: {$ne: 'SHIPPED'},
  });

  if (!loads.length) {
    return res.status(400)
        .json({message: 'No loads were found'});
  }

  return res.json({loads});
};

module.exports.setLoadState = async (req, res) => {
  const {_id: userId} = req.user;
  const states = [
    'EN ROUTE TO PICK UP',
    'ARRIVED TO PICK UP',
    'EN ROUTE TO DELIVERY',
    'ARRIVED TO DELIVERY',
  ];

  const truck = await Truck.findOne({assigned_to: userId});

  if (!truck) {
    return res.status(400)
        .json({message: 'No active truck was found'});
  }

  const load = await Load.findOne({
    assigned_to: truck._id,
    status: {$ne: 'SHIPPED'},
  });

  if (!load) {
    return res.status(400)
        .json({message: 'No active load was found'});
  }

  if (load.state === 'ARRIVED TO DELIVERY') {
    return res.status(400)
        .json('Status is already \'arrived to delivery\'');
  }

  for (let i = 0; i < states.length; i++) {
    if (states[i] === load.state) {
      console.log(load.state);
      load.state = states[i+1];
      load.logs.push({
        message: `Load state changed to ${load.state}`,
        time: Date.now(),
      });
      break;
    }
  }

  if (load.state === 'ARRIVED TO DELIVERY') {
    const truck = await Truck.findOne({assigned_to: userId});

    load.status = 'SHIPPED';
    load.logs.push({
      message: `Load status changed to SHIPPED`,
      time: Date.now(),
    });
    truck.status = 'IS';

    await truck.save();
  }

  await load.save();

  res.json({message: `Load state changed to ${load.state}`});
};

module.exports.getLoadById = async (req, res) => {
  const _id = req.params.id;
  const userId = req.user._id;

  const load = await Load.findOne({_id, created_by: userId});

  if (!load) {
    return res.status(400)
        .json({message: `No load with id: ${id} was found`});
  }

  res.json({load});
};

module.exports.updateLoadInfo = async (req, res) => {
  const _id = req.params.id;
  const newData = req.body;

  const load = await Load.findOne({_id, status: 'NEW'});

  if (!load) {
    return res.status(400)
        .json({message: `No new load with id: ${_id} was found`});
  }

  for (const [key, value] of Object.entries(newData)) {
    if (load[key]) {
      load[key] = value;
    }
  }

  load.logs.push({
    message: `Load ingo was changed`,
    time: Date.now(),
  });

  await load.save();

  res.json({message: 'Load data changed successfully'});
};

module.exports.deleteLoad = async (req, res) => {
  const _id = req.params.id;

  const load = await Load.findOne({_id, status: 'NEW'});

  if (!load) {
    return res.status(400)
        .json({message: `No new load with id: ${_id} was found`});
  }

  await Load.findByIdAndDelete(_id);

  res.json({message: 'Load deleted successfully'});
};

module.exports.postLoad = async (req, res) => {
  const _id = req.params.id;

  const load = await Load.findOne({_id, status: 'NEW'});

  if (!load) {
    return res.status(400)
        .json({message: `No new load with id: ${_id} was found`});
  }

  load.status = 'POSTED';

  load.logs.push({
    message: `Load status changed to POSTED`,
    time: Date.now(),
  });

  let truckType = 'SPRINTER';

  switch (load) {
    case (load.payload <= 2500 &&
      load.dimensions.width <= 500 &&
      load.dimensions.length <= 250 &&
      load.dimensions.height <= 170
    ):
      truckType = 'SMALL STRAIGHT';
      break;
    case (load.payload <= 4000 &&
      load.dimensions.width <= 700 &&
      load.dimensions.length <= 350 &&
      load.dimensions.height <= 200
    ):
      truckType = 'LARGE STRAIGHT';
      break;
    case (load.payload > 4000 &&
      load.dimensions.width > 700 &&
      load.dimensions.length > 350 &&
      load.dimensions.height > 200
    ):
      truckType = 'none';
      break;
    default:
      break;
  }

  let truckArray = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

  if (truckType === 'SMALL STRAIGHT') {
    truckArray = ['SMALL STRAIGHT', 'LARGE STRAIGHT'];
  } else if (truckType === 'LARGE STRAIGHT') {
    truckArray = ['LARGE STRAIGHT'];
  }

  const truck = await Truck.findOne({
    assigned_to: {$ne: null},
    status: 'IS',
    type: {$in: truckArray},
  });

  if (!truck) {
    load.status = 'NEW';
    load.logs.push({
      message: `Truck was not found. Status changed back to new`,
      time: Date.now(),
    });
    await load.save();

    return res.status(400)
        .json({message: 'No driver was found'});
  }

  load.status = 'ASSIGNED';
  load.state = 'EN ROUTE TO PICK UP';
  load.assigned_to = truck._id;
  truck.status = 'OL';

  load.logs.push({
    message: `Load was assigned to truck with id: ${truck._id}`,
    time: Date.now(),
  });

  load.logs.push({
    message: `Load state was changed to: ${load.state}`,
    time: Date.now(),
  });

  await truck.save();
  await load.save();

  res.json({
    message: 'Load posted successfully',
    driver_found: true,
  });
};

module.exports.detailedLoadInfo = async (req, res) => {
  const loadId = req.params.id;
  const userId = req.user._id;

  const load = await Load.findOne({
    _id: loadId,
    created_by: userId,
    status: {$ne: 'NEW'},
  });

  if (!load) {
    return res.status(400)
        .json({message: `No active load with id: ${loadId} was found`});
  }

  const truck = await Truck.findOneById(load.assigned_to);

  res.json({load, truck});
};
