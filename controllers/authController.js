const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  const cryptedPassword = await bcrypt.hash(password, 10);

  const user = new User({
    email,
    role: role.toLowerCase(),
    password: cryptedPassword,
  });

  await user.save();

  res.json({message: 'Profile created successfully'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email '${email}' found`});
  }

  bcrypt.compare(password, user.password, function(err, result) {
    if (err) {
      return res.status(500).json({message: 'Server error!'});
    };

    if (!result) {
      return res.status(400).json({message: 'Wrong password!'});
    }

    const token = jwt.sign({
      email: user.email,
      _id: user._id,
      role: user.role,
    }, JWT_SECRET);

    res.json({jwt_token: token});
  });
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;
  const newPassword = 'testpassword';
  const cryptedPassword = await bcrypt.hash(newPassword, 10);

  const user = await User.findOneAndUpdate({email},
      {password: cryptedPassword});

  if (!user) {
    return res.status(400)
        .json({message: `No user with email '${email}' found`});
  }

  res.json({message: 'New password was sent to your email address'});
};
