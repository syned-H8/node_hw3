const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {
  validateRegister,
  validateLogin,
  validateForgotPassword,
} = require('./middlewares/validateMiddleware');
const {
  registration,
  login,
  forgotPassword,
} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateRegister),
    asyncWrapper(registration));

router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));

router.post('/forgot_password', asyncWrapper(validateForgotPassword),
    asyncWrapper(forgotPassword));

module.exports = router;
