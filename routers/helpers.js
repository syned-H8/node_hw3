/**
 * Wrapper for async function, imit try/catch block
 * @param {object} cb - some function
 * @return {function} - function result
 */
function asyncWrapper(cb) {
  return (req, res, next) => {
    cb(req, res, next)
        .catch(next);
  };
};

module.exports = {
  asyncWrapper,
};
