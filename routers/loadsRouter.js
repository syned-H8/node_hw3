const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  validateAddLoad,
  validateUpdateLoad,
} = require('./middlewares/validateMiddleware');
const {
  addLoad,
  getLoads,
  getActiveLoads,
  setLoadState,
  getLoadById,
  updateLoadInfo,
  deleteLoad,
  postLoad,
  detailedLoadInfo,
} = require('../controllers/loadsController');
const {isShipperMiddleware} = require('./middlewares/isShipperMiddleware');
const {isDriverMiddleware} = require('./middlewares/isDriverMiddleware');

router.use('/', authMiddleware);

router.post('/', isShipperMiddleware,
    asyncWrapper(validateAddLoad), asyncWrapper(addLoad));

router.get('/', asyncWrapper(getLoads));

router.get('/active', isDriverMiddleware, asyncWrapper(getActiveLoads));

router.patch('/active/state', isDriverMiddleware, asyncWrapper(setLoadState));

router.get('/:id', isShipperMiddleware, asyncWrapper(getLoadById));

router.put('/:id', isShipperMiddleware,
    asyncWrapper(validateUpdateLoad), asyncWrapper(updateLoadInfo));

router.delete('/:id', isShipperMiddleware, asyncWrapper(deleteLoad));

router.post('/:id/post', isShipperMiddleware, asyncWrapper(postLoad));

router.get('/:id/shipping_info', isShipperMiddleware,
    asyncWrapper(detailedLoadInfo));

module.exports = router;
