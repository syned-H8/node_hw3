const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  addTruck,
  getUserTrucks,
  getTruckById,
  updateTruckInfo,
  deleteTruck,
  assignTruck,
} = require('../controllers/trucksController');
const {validateTruckType} = require('./middlewares/validateMiddleware');
const {isDriverMiddleware} = require('./middlewares/isDriverMiddleware');
const {isAssignedMiddleware} = require('./middlewares/isAssignedMiddleware');

router.use('/', authMiddleware);
router.use('/', isDriverMiddleware);


router.post('/', asyncWrapper(validateTruckType), asyncWrapper(addTruck));

router.get('/', asyncWrapper(getUserTrucks));

router.get('/:id', asyncWrapper(getTruckById));

router.put('/:id', asyncWrapper(isAssignedMiddleware),
    asyncWrapper(validateTruckType), asyncWrapper(updateTruckInfo));

router.delete('/:id', asyncWrapper(isAssignedMiddleware),
    asyncWrapper(deleteTruck));

router.post('/:id/assign', asyncWrapper(assignTruck));

module.exports = router;
