const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401)
        .json({message: 'No authorization http header found!'});
  }

  const [tokenType, token] = header.split(' ');

  if (!token || !tokenType) {
    return res.status(401)
        .json({message: 'No JWT token found!'});
  }

  req.user = jwt.verify(token, JWT_SECRET);

  next();
};
