const Joi = require('joi');

module.exports.validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),
    role: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required(),
    password: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateForgotPassword = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),

    newPassword: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateTruckType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .pattern(new RegExp(/^(sprinter|small straight|large straight)$/i))
        .required(),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateAddLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number()
          .required(),
      length: Joi.number()
          .required(),
      height: Joi.number()
          .required(),
    }),
  });

  await schema.validateAsync(req.body);

  next();
};

module.exports.validateUpdateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string(),
    payload: Joi.number(),
    pickup_address: Joi.string(),
    delivery_address: Joi.string(),
    dimensions: Joi.object({
      width: Joi.number(),
      length: Joi.number(),
      height: Joi.number(),
    }),
  });

  await schema.validateAsync(req.body);

  next();
};
