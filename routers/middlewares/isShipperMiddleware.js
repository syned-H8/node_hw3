module.exports.isShipperMiddleware = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'shipper') {
    return res.status(400)
        .json({message: 'Available only for shipper role'});
  }

  next();
};
