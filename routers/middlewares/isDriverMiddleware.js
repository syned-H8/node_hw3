module.exports.isDriverMiddleware = (req, res, next) => {
  const {role} = req.user;

  if (role !== 'driver') {
    return res.status(400)
        .json({message: 'Available only for driver role'});
  }

  next();
};
