const {Truck} = require('../../models/truckModel');

module.exports.isAssignedMiddleware = async (req, res, next) => {
  const userId = req.user._id;
  const truckId = req.params.id;

  const truck = await Truck.findOne({_id: truckId, created_by: userId});

  if (!truck) {
    return res.status(400)
        .json({message: `No truck with id: ${truckId} was found as`});
  }

  if (truck.assigned_to) {
    return res.status(400)
        .json({message: 'Available only for not assigned trucks'});
  }

  next();
};
