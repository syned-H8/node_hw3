const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {
  getUserInfo,
  deleteUser,
  submitPassword,
  changePassword,
} = require('../controllers/usersController');
const {validateChangePassword} = require('./middlewares/validateMiddleware');

router.use('/me', authMiddleware);

router.get('/me', asyncWrapper(getUserInfo));

router.delete('/me', asyncWrapper(deleteUser));

router.patch('/me/password', asyncWrapper(validateChangePassword),
    asyncWrapper(submitPassword), asyncWrapper(changePassword));

module.exports = router;
