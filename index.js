require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();
const PORT = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(`mongodb+srv://denys_hrytseniuk:${process.env.DB_PASSWORD}@cluster0.gttaz.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      });

  app.listen(PORT, () => {
    console.log(`Server has been started at port ${PORT}!`);
  });
};

start();
